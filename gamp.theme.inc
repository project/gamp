<?php
/**
 * @file
 * Theme functions for Google AMP.
 *
 * @addtogroup theming
 */

function template_preprocess_gamp_html(&$vars) {
  $vars['content'] = theme('gamp_content', $vars);
}

function template_preprocess_gamp_content(&$vars) {
  $vars['content'] = entity_view($vars['gamp']['type'], [$vars['gamp']['entity']], 'gamp');
}

function template_preprocess_gamp_image(&$vars) {
  if (empty($vars['gamp']['width']) || empty($vars['gamp']['height'])) {
    return '';
  }
}

function template_preprocess_gamp_twitter(&$vars) {
  if (empty($vars['gamp']['status_id'])) {
    return '';
  }
}

function template_preprocess_gamp_youtube(&$vars) {
  if (empty($vars['gamp']['vid'])) {
    return '';
  }
}

function template_preprocess_gamp_facebook(&$vars) {
  if (empty($vars['gamp']['href'])) {
    return '';
  }

  if (empty($vars['gamp']['embed_as'])) {
    $vars['gamp']['embed_as'] = 'post';
  }
}
