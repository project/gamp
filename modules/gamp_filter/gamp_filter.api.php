<?php

/**
 * @file
 * Hooks provided by the gamp_filter.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the blacklist attributes.
 *
 * @param object $whitelist
 */

/**
 * Alter the gamp filter attributes.
 */
function hook_gamp_filter_attr_alter(&$whitelist) {

}

/**
 * @} End of "addtogroup hooks".
 */