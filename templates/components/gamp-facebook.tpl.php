<?php
/**
 * @file
 * The tpl for gamp facebook rendering.
 *
 * Complete documentation for this file is available online.
 * @see https://www.ampproject.org/docs/reference/extended/amp-facebook.html
 */
?>

<amp-facebook
  width=<?php print $gamp['width']; ?> height=<?php print $gamp['height']; ?>
  layout="responsive"
  data-embed-as="<?php print $gamp['embed_as']; ?>"
  data-href="<?php print $gamp['href']; ?>">
</amp-facebook>
