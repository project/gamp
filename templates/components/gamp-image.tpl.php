<?php
/**
 * @file
 * The tpl for gamp image rendering.
 *
 * Complete documentation for this file is available online.
 * @see https://github.com/ampproject/amphtml/blob/master/builtins/amp-img.md
 */
?>

<amp-img
  src="<?php print $gamp['src_url']; ?>"
  <?php if (!empty($gamp['srcset'])): ?>
    srcset="<?php print $gamp['srcset']; ?>"
  <?php endif; ?>
  layout="responsive" placeholder
  width="<?php print $gamp['width']; ?>"
  height="<?php print $gamp['height']; ?>"
  <?php if (!empty($gamp['alt'])): ?>
    alt="<?php print $gamp['alt']; ?>"
  <?php endif; ?>
></amp-img>
