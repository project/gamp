<?php
/**
 * @file
 * The tpl for gamp twitter rendering.
 *
 * Complete documentation for this file is available online.
 * @see https://www.ampproject.org/docs/reference/extended/amp-twitter.html
 */
?>

<amp-twitter
  width=<?php print $gamp['width']; ?>
  height=<?php print $gamp['height']; ?>
  layout="responsive"
  data-tweetid="<?php print $gamp['status_id']; ?>"
>
