<?php
/**
 * @file
 * The tpl for gamp youtube rendering.
 *
 * Complete documentation for this file is available online.
 * @see https://www.ampproject.org/docs/reference/extended/amp-youtube.html
 */
?>

<amp-youtube
  data-videoid="<?php print $gamp['vid']; ?>"
  layout="responsive"
  width="<?php print $gamp['width']; ?>"
  height="<?php print $gamp['height']; ?>"></amp-youtube>
