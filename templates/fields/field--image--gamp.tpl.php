<?php
/**
 * @file
 * The tpl for the gamp image field type.
 *
 * The template will support a standard image field(not rendered)
 *
 * Complete documentation for this file is available online.
 * @see https://github.com/ampproject/amphtml/blob/master/builtins/amp-img.md
 */
?>

<?php foreach ($items as $item) : ?>

  <?php if (!empty($item['#item']['width']) && !empty($item['#item']['height'])): ?>

  <amp-img
    src="<?php print $item['#item']['src_url']; ?>"
    <?php if (!empty($gamp['srcset'])): ?>
      srcset="<?php print $gamp['srcset']; ?>"
    <?php endif; ?>
    layout="responsive" placeholder
    width="<?php print $item['#item']['width']; ?>"
    height="<?php print $item['#item']['height']; ?>"
    alt="<?php print $item['#item']['alt']; ?>">
  </amp-img>
  <?php endif; ?>

<?php endforeach; ?>
