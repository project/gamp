<!doctype html>
<html amp lang="en">
<head>
  <meta charset="utf-8">
  <title><?php print $gamp['title']; ?></title>
  <link rel="canonical" href="<?php print $gamp['canonical']; ?>" />
  <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">

  <?php // Custom CSS ?>
  <?php if (!empty($gamp['style'])): ?>
    <style amp-custom><?php print $gamp['style'] ?></style>
  <?php endif; ?>

  <style>body {opacity: 0}</style><noscript><style>body {opacity: 1}</style></noscript>
  <script async src="https://cdn.ampproject.org/v0.js"></script>

  <?php // Additional scripts. ?>
  <?php foreach (gamp_scripts() as $script) : ?>
    <?php print $script; ?>
  <?php endforeach; ?>
</head>
<body>

  <?php print $content; ?>

</body>
</html>