<?php
/**
 * @file
 * The tpl for the custom gamp image rendering.
 *
 * The template will support a standard image field(not rendered)
 *
 * Complete documentation for this file is available online.
 * @see https://github.com/ampproject/amphtml/blob/master/builtins/amp-img.md
 */
?>

<?php if (!empty($gamp['width']) && !empty($gamp['height'])): ?>

  <amp-img
    src="<?php print $gamp['src_url']; ?>"
    <?php if (!empty($gamp['srcset'])): ?>
      srcset="<?php print $gamp['srcset']; ?>"
    <?php endif; ?>
    layout="responsive" placeholder
    width="<?php print $gamp['width']; ?>"
    height="<?php print $gamp['height']; ?>"
    <?php if (!empty($gamp['alt'])): ?>
      alt="<?php print $gamp['alt']; ?>"
    <?php endif; ?>
  ></amp-img>
<?php endif; ?>
