<?php
/**
 * @file
 * The tpl for the gamp node.
 */
?>

<article>

  <?php
  // We can control what field will be displayed in "gamp" view mode.
  // Otherwise the fields can be hided here.
  ?>

  <?php
  // Hide the comments and links.
  hide($content['comments']);
  hide($content['links']);
  print render($content);
  ?>

</article>
